import os

default_archetype = "archetypes/default.md"
json_archetype = "archetypes/json.md"
toml_archetype = "archetypes/toml.md"

frontmatter_format = "{{ cookiecutter.frontmatter_format }}"

if frontmatter_format == "json":
    os.rename(json_archetype, default_archetype)
    os.remove(toml_archetype)
elif frontmatter_format == "toml":
    os.rename(toml_archetype, default_archetype)
    os.remove(json_archetype)
else:
    os.remove(json_archetype)
    os.remove(toml_archetype)
