{%- if cookiecutter.frontmatter_format == "yaml" -%}
---
title: "Lorem"
date: "2019-12-29T14:25:28-05:00"
draft: false
---
{%- elif cookiecutter.frontmatter_format == "toml" -%}
+++
title = "Lorem"
date = "2019-12-29T14:25:28-05:00"
draft = false
+++
{%- elif cookiecutter.frontmatter_format == "json" -%}
{
  title: "Lorem",
  date: "2019-12-29T14:25:28-05:00",
  draft: false
}
{% endif %}
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
culpa qui officia deserunt mollit anim id est laborum.
