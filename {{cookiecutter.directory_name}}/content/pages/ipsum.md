{%- if cookiecutter.frontmatter_format == "yaml" -%}
---
title: "Ipsum"
date: "2019-12-29T14:25:28-05:00"
draft: false
---
{%- elif cookiecutter.frontmatter_format == "toml" -%}
+++
title = "Ipsum"
date = "2019-12-29T14:25:28-05:00"
draft = false
+++
{%- elif cookiecutter.frontmatter_format == "json" -%}
{
  title: "Ipsum",
  date: "2019-12-29T14:25:28-05:00",
  draft: false
}
{% endif %}
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Vestibulum lectus mauris ultrices eros. Hendrerit dolor magna eget est
lorem. Pharetra et ultrices neque ornare aenean. Aliquam sem fringilla ut morbi tincidunt. Diam
donec adipiscing tristique risus nec feugiat in fermentum posuere. Sed egestas egestas fringilla
phasellus faucibus. Aenean vel elit scelerisque mauris pellentesque pulvinar pellentesque. Ut
venenatis tellus in metus vulputate eu scelerisque felis. In mollis nunc sed id semper. Dui
faucibus in ornare quam viverra orci sagittis. Sed faucibus turpis in eu mi bibendum neque egestas.
