{%- if cookiecutter.frontmatter_format == "yaml" -%}
---
title: "Pages"
date: "2019-12-29T14:25:28-05:00"
draft: false
---
{%- elif cookiecutter.frontmatter_format == "toml" -%}
+++
title = "Pages"
date = "2019-12-29T14:25:28-05:00"
draft = false
+++
{%- elif cookiecutter.frontmatter_format == "json" -%}
{
  title: "Pages",
  date: "2019-12-29T14:25:28-05:00",
  draft: false
}
{% endif %}
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Tellus mauris a diam maecenas sed enim ut sem viverra. Tristique magna sit
amet purus. Auctor neque vitae tempus quam pellentesque nec nam aliquam. Semper quis lectus nulla at
volutpat diam ut venenatis tellus. Lacus luctus accumsan tortor posuere ac ut consequat. Sit amet
consectetur adipiscing elit pellentesque habitant. Leo vel orci porta non pulvinar neque laoreet.
Egestas purus viverra accumsan in nisl. Bibendum at varius vel pharetra vel turpis.
